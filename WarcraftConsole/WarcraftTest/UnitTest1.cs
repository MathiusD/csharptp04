﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Warcraft;
using WarcraftConsole;

namespace WarcraftTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestPeonConstructor()
        {
            Peon grok = new Peon("Grok", "Orc", "Horde", 30, 0);

            Assert.AreEqual("Grok", grok.getName());
            Assert.AreEqual("Orc", grok.getRace());
            Assert.AreEqual("Horde", grok.getFaction());
            Assert.AreEqual(30, grok.getHitPoints());
            Assert.AreEqual(0, grok.getArmor());
        }
        [TestMethod]
        public void TestPeonModification()
        {
            Peon grok = new Peon("Grok", "Ork", "Hord", 320, 60);

            grok.setName("Zark");
            grok.setRace("Orc");
            grok.setFaction("Horde");
            grok.setHitPoints(30);
            grok.setArmor(0);

            Assert.AreEqual("Zark", grok.getName());
            Assert.AreEqual("Orc", grok.getRace());
            Assert.AreEqual("Horde", grok.getFaction());
            Assert.AreEqual(30, grok.getHitPoints());
            Assert.AreEqual(0, grok.getArmor());
        }
        [TestMethod]
        public void TestPeonSayHello()
        {
            Peon grok = new Peon("Grok", "Orc", "Horde", 30, 0);
            
            Assert.AreEqual("Je suis Orc de Horde, j'ai 30 Hit points et 0 d'Armor.", grok.sayHello());
        }
        [TestMethod]
        public void TestPeontalk()
        {
            Peon grok = new Peon("Grok", "Orc", "Horde", 30, 0);

            Assert.AreEqual("Occupé ! Oubliez-moi !", grok.talk());
        }
        [TestMethod]
        public void TestPeongrunt()
        {
            Peon grok = new Peon("Grok", "Orc", "Horde", 30, 0);

            Assert.AreEqual("Zog Zog!", grok.grunt());
        }
        [TestMethod]
        public void TestPeontalkToPeasant()
        {
            Peasant louis = new Peasant("Louis", "Humain", "Alliance", 30, 0);
            Peon grok = new Peon("Grok", "Orc", "Horde", 30, 0);

            Assert.AreEqual("Tuez-le Humain ! Lok Tar Ogar pour la Horde !", grok.talkToPeasant(louis));
        }
        [TestMethod]
        public void TestPeontalkToPeon()
        {
            Peon zark = new Peon("Zark", "Orc", "Horde", 30, 0);
            Peon grok = new Peon("Grok", "Orc", "Horde", 30, 0);

            Assert.AreEqual("Je ne suis pas un gentil Orc ! Espece de Orc !", grok.talkToPeon(zark));
        }
        [TestMethod]
        public void TestPeasantConstructor()
        {
            Peasant louis = new Peasant("Louis", "Humain", "Alliance", 30, 0);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Humain", louis.getRace());
            Assert.AreEqual("Alliance", louis.getFaction());
            Assert.AreEqual(30, louis.getHitPoints());
            Assert.AreEqual(0, louis.getArmor());
        }
        [TestMethod]
        public void TestPeasantModification()
        {
            Peasant louis = new Peasant("Louis", "Humain", "Alliace", 320, 60);

            louis.setName("Torin, Barbe Etincelante");
            louis.setRace("Nain");
            louis.setFaction("Alliance");
            louis.setHitPoints(30);
            louis.setArmor(0);

            Assert.AreEqual("Torin, Barbe Etincelante", louis.getName());
            Assert.AreEqual("Nain", louis.getRace());
            Assert.AreEqual("Alliance", louis.getFaction());
            Assert.AreEqual(30, louis.getHitPoints());
            Assert.AreEqual(0, louis.getArmor());
        }
        [TestMethod]
        public void TestPeasantSayHello()
        {
            Peasant louis = new Peasant("Louis", "Humain", "Alliance", 30, 0);

            Assert.AreEqual("Je suis Humain de Alliance, j'ai 30 Hit points et 0 d'Armor.", louis.sayHello());
        }
        [TestMethod]
        public void TestPeasanttalk()
        {
            Peasant louis = new Peasant("Louis", "Humain", "Alliance", 30, 0);

            Assert.AreEqual("Qui y a-t-il ? Encore du travail ?", louis.talk());
        }
        [TestMethod]
        public void TestPeasantgrunt()
        {
            Peasant louis = new Peasant("Louis", "Humain", "Alliance", 30, 0);

            Assert.AreEqual("Point Travailler.", louis.grunt());
        }
        [TestMethod]
        public void TestPeasanttalkToPeasant()
        {
            Peasant louis = new Peasant("Louis", "Humain", "Alliance", 30, 0);
            Peasant torin = new Peasant("Torin, Barbe Etincelante", "Nain", "Alliace", 30, 0);

            Assert.AreEqual("Bonjour je suis Louis et vous un Nain, je coupe du bois et vous ?", louis.talkToPeasant(torin));
        }
        [TestMethod]
        public void TestPeasanttalkToPeon()
        {
            Peasant louis = new Peasant("Louis", "Humain", "Alliance", 30, 0);
            Peon grok = new Peon("Grok", "Orc", "Horde", 30, 0);

            Assert.AreEqual("Contemple la puissance de Alliance ! La Horde perira", louis.talkToPeon(grok));
        }
    }
}
