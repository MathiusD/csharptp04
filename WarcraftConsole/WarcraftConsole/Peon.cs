﻿using System;

namespace Warcraft
{
    public class Peon
    {
        private string name;
        private string race;
        private string faction;
        private int hitpoints;
        private int armor;


        public Peon(string name, string race, string faction, int hitpoints, int armor)
        {
            this.name = name;
            this.race = race;
            this.faction = faction;
            this.hitpoints = hitpoints;
            this.armor = armor;

        }

        public string sayHello()
        {
            return string.Format("Je suis {0} de {1}, j'ai {2} Hit points et {3} d'Armor.", this.race, this.faction, this.hitpoints, this.armor);

        }

        public string talk()
        {
            return "Occupé ! Oubliez-moi !";
        }
        public string grunt()
        {
            return "Zog Zog!";
        }
        public string getName()
        {
            return this.name;
        }
        public string getRace()
        {
            return this.race;
        }
        public string getFaction()
        {
            return this.faction;
        }
        public int getHitPoints()
        {
            return this.hitpoints;
        }
        public int getArmor()
        {
            return this.armor;
        }
        public void setName(string name)
        {
            this.name = name;
        }
        public void setRace(string race)
        {
            this.race = race;
        }
        public void setFaction(string faction)
        {
            this.faction = faction;
        }
        public void setHitPoints(int hitpoints)
        {
            this.hitpoints = hitpoints;
        }
        public void setArmor(int armor)
        {
            this.armor = armor;
        }
        public string talkToPeasant(Peasant peasant)
        {
            return string.Format("Tuez-le {0} ! Lok Tar Ogar pour la {1} !", peasant.getRace(), this.faction);
        }
        public string talkToPeon(Peon peon)
        {
            return string.Format("Je ne suis pas un gentil {0} ! Espece de {1} !", this.race, peon.getRace());
        }
    }
}
