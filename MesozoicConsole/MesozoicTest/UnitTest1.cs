﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MesozoicConsole;
using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Stegausaurus", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Grrr", louis.roar());
        }

        [TestMethod]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }
        [TestMethod]
        public void TestDinosaurModification()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            louis.setName("Nessie");
            louis.setSpecie("Diplodocus");
            louis.setAge(11);

            Assert.AreEqual("Nessie", louis.getName());
            Assert.AreEqual("Diplodocus", louis.getSpecie());
            Assert.AreEqual(11, louis.getAge());
        }
        [TestMethod]
        public void TestDinosaurHug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);

            Assert.AreEqual("Je suis Louis et je fais un calin à Nessie.", louis.hug(nessie));
        }
    }
}
