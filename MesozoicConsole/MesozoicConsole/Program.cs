﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Mesozoic;

namespace MesozoicConsole
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Console.WriteLine("Louis est :");
            Console.WriteLine(louis.getName());
            Console.WriteLine(louis.getSpecie());
            Console.WriteLine(louis.getAge());
            Console.WriteLine(nessie.sayHello());
            Console.WriteLine(nessie.roar());
            Console.WriteLine(nessie.hug(louis));
            Console.ReadKey();
        }
    }
}
