﻿using System;

namespace Mesozoic
{
    public class Dinosaur
    {
        private string name;
        private string specie;
        private int age;


        public Dinosaur(string name, string specie, int age)
        {
            this.name = name;
            this.specie = specie;
            this.age = age;
        }

        public string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, this.specie, this.age);
        }

        public string roar()
        {
            return "Grrr";
        }
        public string getName()
        {
            return this.name;
        }
        public string getSpecie()
        {
            return this.specie;
        }
        public int getAge()
        {
            return this.age;
        }
        public void setName(string name)
        {
            this.name = name;
        }
        public void setSpecie(string specie)
        {
            this.specie = specie;
        }
        public void setAge(int age)
        {
            this.age = age;
        }
        public string hug(Dinosaur otherdino)
        {
            return string.Format("Je suis {0} et je fais un calin à {1}.", this.name, otherdino.getName());
        }
    }
}